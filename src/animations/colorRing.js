export default function colorRing(color) {
	return [...new Array(60).keys()].map(() => color);
}
