export default function colorfulGradient(n) {
	let r = 255;
	let g = 0;
	let b = 0;

	let rAdd = -1;
	let gAdd = +1;
	let bAdd = 0;

	const value = (255 / 60) * 3;
	const pixels = [];

	for (let pos = 0; pos < n; pos++) {
		if (r < value && rAdd === -1) {
			rAdd = 0;
		} else if (r > 255 - value && rAdd === +1) {
			rAdd = -1;
			gAdd = +1;
		}

		if (g < value && gAdd === -1) {
			gAdd = 0;
		} else if (g > 255 - value && gAdd === +1) {
			gAdd = -1;
			bAdd = +1;
		}

		if (b < value && bAdd === -1) {
			bAdd = 0;
		} else if (b > 255 - value && bAdd === +1) {
			bAdd = -1;
			rAdd = +1;
		}

		r += rAdd * value;
		g += gAdd * value;
		b += bAdd * value;

		pixels.push(`rgb(${Math.round(r)}, ${Math.round(g)}, ${Math.round(b)})`);
	}

	return pixels;
}
