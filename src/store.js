import Vue from 'vue';
import Vuex from 'vuex';
import debounce from 'lodash.debounce';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		host: localStorage.getItem('stripperServer') || false,
		ssl: localStorage.getItem('stripperServerSSL') || false,
		brightness: 255,
		on: false,
		colorPicker: false,
		color: [255, 255, 255],
		gradient: -1,
		mode: 0,
		speed: 100
	},
	mutations: {
		toggleBulb(state) {
			allowSync = false;
			state.on = !state.on;
		},
		setBrightness(state, brightness) {
			allowSync = false;
			state.brightness = brightness;
		},
		toggleColorPicker(state) {
			state.colorPicker = !state.colorPicker;
		},
		setColor(state, rgb) {
			allowSync = false;
			state.color = rgb;
		},
		setGradient(state, gradient) {
			allowSync = false;
			state.gradient = gradient;
		},
		setMode(state, mode) {
			allowSync = false;
			state.mode = mode;
		},
		setSpeed(state, speed) {
			allowSync = false;
			state.speed = speed;
		},
		syncServer(state, data) {
			console.log('server state', data.on);
			Object.keys(data).forEach(key => {
				Vue.set(state, key, data[key]);
			});
			console.log('sync state', state.on);
		},
		setHost(state, host) {
			state.host = host;
			localStorage.setItem('stripperServer', host);
			connect();
		},
		setSSL(state, ssl) {
			state.ssl = ssl;
			localStorage.setItem('stripperServerSSL', ssl);
		}
	}
});

let socket;
let socketAttempts = 0;
const connect = () => {
	if (!store.state.host) return;

	socket = new WebSocket(
		`ws${store.state.ssl ? 's' : ''}://${store.state.host}/socket`
	);
	socket.addEventListener('error', () => store.commit('setHost', false));
	socket.addEventListener('close', () => {
		if (socketAttempts < 2) {
			socketAttempts++;
			connect();
		} else {
			socketAttempts = 0;
			store.commit('setHost', false);
		}
	});

	socket.addEventListener('message', ({ data }) => {
		console.log(allowSync, data);
		if (!allowSync) return;

		try {
			const json = JSON.parse(data);
			store.commit('syncServer', json);
			console.log(json);
		} catch (error) {
			console.error(error);
		}
	});
};

connect();

let readyToFetch = true;
let previousState = Object.assign({}, store.state);
let allowSync = true;

store.subscribe(
	debounce(
		async (mutation, state) => {
			if (mutation.type === 'syncServer') return;
			if (readyToFetch && socket.readyState === 1) {
				readyToFetch = false;
				const diff = {};
				Object.keys(state).forEach(key => {
					if (state[key] !== previousState[key]) {
						diff[key] = state[key];
					}
				});

				socket.send(JSON.stringify(state));
				console.log('sent', diff);
				readyToFetch = true;
				previousState = Object.assign({}, state);
				console.log(allowSync);
				allowSync = true;
			}
		},
		50,
		{
			leading: true,
			trailing: true
		}
	)
);

export default store;
