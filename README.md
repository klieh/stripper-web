# stripper

[![pipeline status](https://gitlab.com/klieh/stripper-web/badges/master/pipeline.svg)](https://gitlab.com/klieh/stripper-web/commits/master)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn dev
```

### Compiles and minifies for production

```
yarn build
```

### Run your tests

```
yarn test
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
